/**
 * @file dsort.c
 * @author Alija-Ali Abadzic 01427575 <e01427575@student.tuwien.ac.at>
 * @date 23.11.2017
 *
 * @brief Main program module.
 * 
 * This program execute two commands, read their output and save it
 * in one array. After that array should be sorted and to uniq command sent
 * and uniq should print only duplicates of this array members
 **/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>

/**
 * Maximum line length
 */
#define MAX_LINE_LEN 1023


/**
 * Global variable
 * @brief Pointer to programm name
 **/
char *prog_name = "<not_set>";


/**
 * Mandatory usage function.
 * @brief This function writes helpful usage information about the program to stderr.
 * @details global variables: prog_name
 **/
static void usage(){
    (void) fprintf(stderr,"%s command1 command2 \n", prog_name);
    exit(EXIT_FAILURE);
}

/**
 * Compare two strings
 * @brief This function comapre two string and is used in qsort algorithm
 * @return int value depending on is a bigger then b or not or are they equal
 **/
static int line_compare(const void * a, const void * b){
   
     char  *pa = (*(char**)a);
     char  *pb = (*(char**)b);

     char *ca = strdup(pa);
     char *cb = strdup(pb);

    for(int i = 0; i<strlen(ca); i++){
        ca[i] = tolower(ca[i]);
    }
    
    for(int i = 0; i<strlen(cb); i++){
        cb[i] = tolower(cb[i]);
    }
    
    return strcmp(ca,cb);
}

/**
* @brief Program entry point.
* @param argc The argument counter.
* @param argv The argument vector.
* @return EXIT_FAILURE on failure or EXIT_SUCCESS on success.
*/
int main(int argc, char **argv){

    char **array;
    char line[1023];
    int lineNum;
    array = (char **) malloc(1 * sizeof(char *));
    int opt;
    if(argc > 0){
        prog_name = argv[0];
    }

    if(argc > 3){
        usage();
    }

    while((opt = getopt(argc,argv,"")) != -1){
        switch(opt){
            case '?':
                usage();
                break;
            default:
                usage();
                break;
        }
    }

    char *cmd1 = argv[1];
    char *cmd2 = argv[2];

    int pipefd1[2];
    if(pipe(pipefd1) < 0){
        (void) fprintf(stderr,"Error occuerd during creation of pipe \n");
        exit(EXIT_FAILURE);
    }
    
    pid_t pid1 = fork();
    if (pid1 < 0){
        (void) fprintf(stderr,"Error occuerd at fork() call \n");
        exit(EXIT_FAILURE);
    }
    if(pid1 == 0){
        /* 1. Child process */
        // close unused read end
        close(pipefd1[0]);
        // duplicate write end to stdout(and close implicite stdout fd)
        if(dup2(pipefd1[1],1) == -1){
            (void) fprintf(stderr,"Error occuerd at dup2() call in child process \n");
            exit(EXIT_FAILURE);
        }
        
        // close write end
        close(pipefd1[1]);
        
        if(execl("/bin/bash","bash", "-c",cmd1,NULL) == -1){
            (void) fprintf(stderr,"Error occuerd at execl() call in 1. child process \n");
            exit(EXIT_FAILURE);
        }
    }
    else{

        /* Parent process */
        // close unused write end
        close(pipefd1[1]);
        // duplicate read end to stdin(and close implicite stdin fd)
        if(dup2(pipefd1[0],0) == -1){
            (void) fprintf(stderr,"Error occuerd at dup2() call in parent process \n");
            exit(EXIT_FAILURE);
        }

        FILE *filep1;
        filep1 = fdopen(pipefd1[0], "r");

        (void) waitpid(pid1, NULL, 0);
        


        //now we do another pipe and fork for cmd2



        int pipefd2[2];
        if(pipe(pipefd2) < 0){
            (void) fprintf(stderr,"Error occuerd during creation of pipe2 \n");
            exit(EXIT_FAILURE);
        }
        // fork call for 2. child process
        pid_t pid2 = fork();

        if (pid2 < 0)
        {
            (void)fprintf(stderr, "Error occuerd at fork() call \n");
            exit(EXIT_FAILURE);
        }
        if(pid2 == 0)
        {
            /* 2. Child process */
            // close unused read end
            close(pipefd2[0]);
            // duplicate write end to stdout(and close stdout fd)
            if (dup2(pipefd2[1], 1) == -1)
            {
                (void)fprintf(stderr, "Error occuerd at dup2() call in child process \n");
                exit(EXIT_FAILURE);
            }

            // close write end
            close(pipefd2[1]);

            if (execl("/bin/bash", "bash", "-c", cmd2, NULL) == -1)
            {
                (void)fprintf(stderr, "Error occuerd at execl() call in 2. child process \n");
                exit(EXIT_FAILURE);
            }
        }
        else
        {
            /* Parent process */
            // close unused write end
            close(pipefd2[1]);
            // duplicate read end to stdin(and close stdin fd)
            if (dup2(pipefd2[0], 0) == -1)
            {
                (void)fprintf(stderr, "Error occuerd at dup2() call in 2. parent process \n");
                exit(EXIT_FAILURE);
            }

            FILE *filep2;
            filep2 = fdopen(pipefd2[0], "r");
            int i = 0;
            while (!feof(filep1))
            {
                if (fgets(line, 1023, filep1) != NULL)
                {
                    char *temp = strdup(line);
                    array[i] = temp;
                    // testings printf("cmd1: %s", array[i]);
                    i++;
                    lineNum++;
                    array = (char **) realloc(array,lineNum * sizeof(char *));
                    
                }
            }
            while (!feof(filep2))
            {
                if (fgets(line, 1023, filep2) != NULL)
                {   
                    char *temp = strdup(line);                    
                    array[i] = temp;
                    // testing printf("cmd2: %s",array[i]);
                    i++;
                    lineNum++;
                    array = (char **) realloc(array,lineNum * sizeof(char *));                    
                }
            }
            for (int j = 0; j < i; j++)
            {
               // testing printf("%s", array[j]);
            }
            qsort(array,i,sizeof(char *),line_compare);
            //printf("after sort: \n");
            for (int j = 0; j < i; j++)
            {
             //   printf("%d %s",j,array[j]);
            }

            // close read ends of pipe1 and pipe2
            close(pipefd1[0]);
            close(pipefd2[0]);
            (void)waitpid(pid2, NULL, 0);
            
            
            // now we do another pipe and fork for uniq


    
            int pipefd3[2];
            if(pipe(pipefd3) < 0){
                (void) fprintf(stderr,"Error occuerd during creation of pipe3 \n");
                exit(EXIT_FAILURE);
            }
            // fork call for 3. child process
            
            pid_t pid3 = fork();
            
            if (pid3 < 0)
            {
                
                (void)fprintf(stderr, "Error occuerd at fork() call \n");
                exit(EXIT_FAILURE);
            }
            if(pid3 == 0)
            {
                
                /* 3. Child process */
                // close unused write end
                close(pipefd3[1]);
                // duplicate read end to stdin(and close stdin fd)
                if (dup2(pipefd3[0], 0) == -1)
                {
                    (void)fprintf(stderr, "Error occuerd at dup2() call in child process \n");
                    exit(EXIT_FAILURE);
                }
            
                // close read end
                close(pipefd3[0]);
                
                if (execl("/bin/bash", "bash", "-c", "uniq -d", NULL) == -1)
                {
                    (void)fprintf(stderr, "Error occuerd at execl() call in 2. child process \n");
                    exit(EXIT_FAILURE);
                }

            }
            else
            {   
                /* Parent process */
                // close read end
                close(pipefd3[0]);
                // printf("in parent \n");
                for (int j = 0; j < i; j++)
                {
                   // printf("%d %s",j,array[j]);
                    write(pipefd3[1],array[j],strlen(array[j]));
                }
                
                // close write end
                close(pipefd3[1]);
                (void)waitpid(pid3, NULL, 0);
            }
        }

    }

// free resources
free(array);
return 0;
}
